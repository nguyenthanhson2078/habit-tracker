let progress1 = 0;
let spinnerBar1 = document.getElementsByClassName("spinner-bar")[0];
let middleProgress1 = document.getElementsByClassName("middle-progress")[0];
let minus1 = document.getElementsByClassName("tag-one-minus");
let mark1 = document.getElementsByClassName("tag-one-mark");
let check1 = document.getElementsByClassName("tag-one-check");

for (let i = 0; i < mark1.length; i++) {
    mark1[i].onclick = function() {
        if ((mark1[i].style.display = "inline")) {
            mark1[i].style.display = "none";
            check1[i].style.visibility = "visible";
        }
        if (progress1 != 100) {
            progress1 = progress1 + 25;
            setProgress1();
        }
    };
    check1[i].onclick = function() {
        if ((check1[i].style.visibility = "visible")) {
            check1[i].style.visibility = "hidden";
            minus1[i].style.visibility = "visible";
        }
        if (progress1 != 0) {
            progress1 = progress1 - 25;
            setProgress1();
        }
    };
    minus1[i].onclick = function() {
        if ((minus1[i].style.visibility = "visible")) {
            minus1[i].style.visibility = "hidden";
            mark1[i].style.display = "inline";
        }
    };
}

function setProgress1() {
    spinnerBar1.style.background =
        "conic-gradient(rgb(240 230 140)" +
        progress1 +
        "%,rgb(91,91,91)" +
        progress1 +
        "%";
    middleProgress1.innerHTML = progress1.toString() + "%";
}

let progress2 = 0;
let spinnerBar2 = document.getElementsByClassName("spinner-bar")[1];
let middleProgress2 = document.getElementsByClassName("middle-progress")[1];
let minus2 = document.getElementsByClassName("tag-two-minus");
let mark2 = document.getElementsByClassName("tag-two-mark");
let check2 = document.getElementsByClassName("tag-two-check");

for (let i = 0; i < mark2.length; i++) {
    mark2[i].onclick = function() {
        if ((mark2[i].style.display = "inline")) {
            mark2[i].style.display = "none";
            check2[i].style.visibility = "visible";
        }
        if (progress2 != 100) {
            progress2 = progress2 + 25;
            setProgress2();
        }
    };
    check2[i].onclick = function() {
        if ((check2[i].style.visibility = "visible")) {
            check2[i].style.visibility = "hidden";
            minus2[i].style.visibility = "visible";
        }
        if (progress2 != 0) {
            progress2 = progress2 - 25;
            setProgress2();
        }
    };
    minus2[i].onclick = function() {
        if ((minus2[i].style.visibility = "visible")) {
            minus2[i].style.visibility = "hidden";
            mark2[i].style.display = "inline";
        }
    };
}

function setProgress2() {
    spinnerBar2.style.background =
        "conic-gradient(rgb(240 230 140)" +
        progress2 +
        "%,rgb(91,91,91)" +
        progress2 +
        "%";
    middleProgress2.innerHTML = progress2.toString() + "%";
}

let progress3 = 0;
let spinnerBar3 = document.getElementsByClassName("spinner-bar")[2];
let middleProgress3 = document.getElementsByClassName("middle-progress")[2];
let minus3 = document.getElementsByClassName("tag-three-minus");
let mark3 = document.getElementsByClassName("tag-three-mark");
let check3 = document.getElementsByClassName("tag-three-check");

for (let i = 0; i < mark3.length; i++) {
    mark3[i].onclick = function() {
        if ((mark3[i].style.display = "inline")) {
            mark3[i].style.display = "none";
            check3[i].style.visibility = "visible";
        }
        if (progress3 != 100) {
            progress3 = progress3 + 25;
            setProgress3();
        }
    };
    check3[i].onclick = function() {
        if ((check3[i].style.visibility = "visible")) {
            check3[i].style.visibility = "hidden";
            minus3[i].style.visibility = "visible";
        }
        if (progress3 != 0) {
            progress3 = progress3 - 25;
            setProgress3();
        }
    };
    minus3[i].onclick = function() {
        if ((minus3[i].style.visibility = "visible")) {
            minus3[i].style.visibility = "hidden";
            mark3[i].style.display = "inline";
        }
    };
}

function setProgress3() {
    spinnerBar3.style.background =
        "conic-gradient(rgb(240 230 140)" +
        progress3 +
        "%,rgb(91,91,91)" +
        progress3 +
        "%";
    middleProgress3.innerHTML = progress3.toString() + "%";
}

let progress4 = 0;
let spinnerBar4 = document.getElementsByClassName("spinner-bar")[3];
let middleProgress4 = document.getElementsByClassName("middle-progress")[3];
let minus4 = document.getElementsByClassName("tag-four-minus");
let mark4 = document.getElementsByClassName("tag-four-mark");
let check4 = document.getElementsByClassName("tag-four-check");

for (let i = 0; i < mark4.length; i++) {
    mark4[i].onclick = function() {
        if ((mark4[i].style.display = "inline")) {
            mark4[i].style.display = "none";
            check4[i].style.visibility = "visible";
        }
        if (progress4 != 100) {
            progress4 = progress4 + 25;
            setProgress4();
        }
    };
    check4[i].onclick = function() {
        if ((check4[i].style.visibility = "visible")) {
            check4[i].style.visibility = "hidden";
            minus4[i].style.visibility = "visible";
        }
        if (progress4 != 0) {
            progress4 = progress4 - 25;
            setProgress4();
        }
    };
    minus4[i].onclick = function() {
        if ((minus4[i].style.visibility = "visible")) {
            minus4[i].style.visibility = "hidden";
            mark4[i].style.display = "inline";
        }
    };
}

function setProgress4() {
    spinnerBar4.style.background =
        "conic-gradient(rgb(63, 243, 156)" +
        progress4 +
        "%,rgb(91,91,91)" +
        progress4 +
        "%";
    middleProgress4.innerHTML = progress4.toString() + "%";
}

let progress5 = 0;
let spinnerBar5 = document.getElementsByClassName("spinner-bar")[4];
let middleProgress5 = document.getElementsByClassName("middle-progress")[4];
let minus5 = document.getElementsByClassName("tag-five-minus");
let mark5 = document.getElementsByClassName("tag-five-mark");
let check5 = document.getElementsByClassName("tag-five-check");

for (let i = 0; i < mark5.length; i++) {
    mark5[i].onclick = function() {
        if ((mark5[i].style.display = "inline")) {
            mark5[i].style.display = "none";
            check5[i].style.visibility = "visible";
        }
        if (progress5 != 100) {
            progress5 = progress5 + 25;
            setProgress5();
        }
    };
    check5[i].onclick = function() {
        if ((check5[i].style.visibility = "visible")) {
            check5[i].style.visibility = "hidden";
            minus5[i].style.visibility = "visible";
        }
        if (progress5 != 0) {
            progress5 = progress5 - 25;
            setProgress5();
        }
    };
    minus5[i].onclick = function() {
        if ((minus5[i].style.visibility = "visible")) {
            minus5[i].style.visibility = "hidden";
            mark5[i].style.display = "inline";
        }
    };
}

function setProgress5() {
    spinnerBar5.style.background =
        "conic-gradient(rgb(63, 243, 156)" +
        progress5 +
        "%,rgb(91,91,91)" +
        progress5 +
        "%";
    middleProgress5.innerHTML = progress5.toString() + "%";
}

let progress6 = 0;
let spinnerBar6 = document.getElementsByClassName("spinner-bar")[5];
let middleProgress6 = document.getElementsByClassName("middle-progress")[5];
let minus6 = document.getElementsByClassName("tag-six-minus");
let mark6 = document.getElementsByClassName("tag-six-mark");
let check6 = document.getElementsByClassName("tag-six-check");

for (let i = 0; i < mark6.length; i++) {
    mark6[i].onclick = function() {
        if ((mark6[i].style.display = "inline")) {
            mark6[i].style.display = "none";
            check6[i].style.visibility = "visible";
        }
        if (progress6 != 100) {
            progress6 = progress6 + 25;
            setProgress6();
        }
    };
    check6[i].onclick = function() {
        if ((check6[i].style.visibility = "visible")) {
            check6[i].style.visibility = "hidden";
            minus6[i].style.visibility = "visible";
        }
        if (progress6 != 0) {
            progress6 = progress6 - 25;
            setProgress6();
        }
    };
    minus6[i].onclick = function() {
        if ((minus6[i].style.visibility = "visible")) {
            minus6[i].style.visibility = "hidden";
            mark6[i].style.display = "inline";
        }
    };
}

function setProgress6() {
    spinnerBar6.style.background =
        "conic-gradient(rgb(63, 243, 156)" +
        progress6 +
        "%,rgb(91,91,91)" +
        progress6 +
        "%";
    middleProgress6.innerHTML = progress6.toString() + "%";
}

let progress7 = 0;
let spinnerBar7 = document.getElementsByClassName("spinner-bar")[6];
let middleProgress7 = document.getElementsByClassName("middle-progress")[6];
let minus7 = document.getElementsByClassName("tag-seven-minus");
let mark7 = document.getElementsByClassName("tag-seven-mark");
let check7 = document.getElementsByClassName("tag-seven-check");

for (let i = 0; i < mark7.length; i++) {
    mark7[i].onclick = function() {
        if ((mark7[i].style.display = "inline")) {
            mark7[i].style.display = "none";
            check7[i].style.visibility = "visible";
        }
        if (progress7 != 100) {
            progress7 = progress7 + 25;
            setProgress7();
        }
    };
    check7[i].onclick = function() {
        if ((check7[i].style.visibility = "visible")) {
            check7[i].style.visibility = "hidden";
            minus7[i].style.visibility = "visible";
        }
        if (progress7 != 0) {
            progress7 = progress7 - 25;
            setProgress7();
        }
    };
    minus7[i].onclick = function() {
        if ((minus7[i].style.visibility = "visible")) {
            minus7[i].style.visibility = "hidden";
            mark7[i].style.display = "inline";
        }
    };
}

function setProgress7() {
    spinnerBar7.style.background =
        "conic-gradient(rgb(51, 149, 235)" +
        progress7 +
        "%,rgb(91,91,91)" +
        progress7 +
        "%";
    middleProgress7.innerHTML = progress7.toString() + "%";
}

let progress8 = 0;
let spinnerBar8 = document.getElementsByClassName("spinner-bar")[7];
let middleProgress8 = document.getElementsByClassName("middle-progress")[7];
let minus8 = document.getElementsByClassName("tag-eight-minus");
let mark8 = document.getElementsByClassName("tag-eight-mark");
let check8 = document.getElementsByClassName("tag-eight-check");

for (let i = 0; i < mark8.length; i++) {
    mark8[i].onclick = function() {
        if ((mark8[i].style.display = "inline")) {
            mark8[i].style.display = "none";
            check8[i].style.visibility = "visible";
        }
        if (progress8 != 100) {
            progress8 = progress8 + 25;
            setProgress8();
        }
    };
    check8[i].onclick = function() {
        if ((check8[i].style.visibility = "visible")) {
            check8[i].style.visibility = "hidden";
            minus8[i].style.visibility = "visible";
        }
        if (progress8 != 0) {
            progress8 = progress8 - 25;
            setProgress8();
        }
    };
    minus8[i].onclick = function() {
        if ((minus8[i].style.visibility = "visible")) {
            minus8[i].style.visibility = "hidden";
            mark8[i].style.display = "inline";
        }
    };
}

function setProgress8() {
    spinnerBar8.style.background =
        "conic-gradient(rgb(142, 22, 255)" +
        progress8 +
        "%,rgb(91,91,91)" +
        progress8 +
        "%";
    middleProgress8.innerHTML = progress8.toString() + "%";
}

let modal = document.getElementsByClassName("modal")[0];
let plus = document.getElementsByClassName("fa-plus")[0];

plus.onclick = function() {
    modal.style.display = "block";
};
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

let menu1 = document.getElementsByClassName("menu-one")[0];
let board1 = document.getElementsByClassName("board-one_information")[0];
let back = document.getElementsByClassName("fa-arrow-left")[0];
menu1.onclick = function() {
    board1.style.display = "block";
}
back.onclick = function() {
    if (modal.style.display = "block") {
        board1.style.display = "none";
        modal.style.display = "none";
        warningIcon1.style.display = "none";
        warningBox1.style.display = "none";
    }
}

let save1 = document.getElementsByClassName("board-one-save_input")[0];
let warningIcon1 = document.getElementsByClassName("board-one-circle_error")[0];
let warningBox1 = document.getElementsByClassName("board-one-box_error")[0];
let nameBoard1 = document.getElementsByClassName("board-one-name_placeholder")[0];
let questionBoard1 = document.getElementsByClassName("board-one-name_placeholder")[1];
let noteBoard1 = document.getElementsByClassName("board-one-name_placeholder")[2];
save1.onclick = function() {
    if (nameBoard1.value === "") {
        warningIcon1.style.display = "block";
        warningBox1.style.display = "block";
    } else {
        board1.style.display = "none";
        modal.style.display = "none";
        warningIcon1.style.display = "none";
        warningBox1.style.display = "none";
        nameBoard1.value = "";
    }
}
nameBoard1.onkeypress = function() {
    if (nameBoard1.value === "") {
        warningIcon1.style.display = "none";
        warningBox1.style.display = "none";
    }
}
nameBoard1.onclick = function() {
    if (warningBox1.style.display = "none")
        warningBox1.style.display = "block";
}
questionBoard1.onclick = function() {
    if (warningBox1.style.display = "block") {
        warningBox1.style.display = "none";
    }
}
noteBoard1.onclick = function() {
    if (warningBox1.style.display = "block") {
        warningBox1.style.display = "none";
    }
}